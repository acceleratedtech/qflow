#!/bin/bash

./configure --prefix=$PWD/build || exit 1
make || exit 1
make install || exit 1

if [ -d openmsp430 ]; then
    true
else
    git clone --depth 1 https://acceleratedtech@bitbucket.org/acceleratedtech/openmsp430.git || exit 2
fi

export PATH=$PWD/build/bin:$PATH
cd openmsp430/qflow
ln -sfv ../core/rtl/verilog source
qflow synthesize place route openMSP430 > qflow.log 2>&1; status=$?
if [ "$status" == "0" ]; then
    echo "Build succeeded!"
else
    echo "Build failed!" >&2
fi


